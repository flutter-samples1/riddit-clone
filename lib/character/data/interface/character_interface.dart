import '../model/character.dart';

abstract class CharacterInterface {
  Future<List<Character>> getCharacters({required int page});
}
