import 'package:graphql_flutter/graphql_flutter.dart';

import '../model/character.dart';

enum _QueryName {
  characters,
}

class CharacterRemoteSource {
  final GraphQLClient _graphQLClient;

  const CharacterRemoteSource(this._graphQLClient);

  static const _charactersQuery = r'''
    query Characters($page: Int) {
      characters(page: $page) {
        results {
          id
          name
          species
          image
          created
        }
      }
    }
  ''';

  Future<List<Character>> getCharacters(int page) async {
    final options = QueryOptions(
      document: gql(_charactersQuery),
      variables: {'page': page},
    );

    final response = await _graphQLClient.query(options);

    if (!response.hasException) {
      final data = response.data![_QueryName.characters.name]['results']
          as List<dynamic>;

      return data.map((e) => Character.fromJson(e)).toList();
    } else {
      throw response.exception!;
    }
  }
}
