import '../interface/character_interface.dart';
import '../model/character.dart';
import '../remote/character_remote_source.dart';

class CharacterRepository implements CharacterInterface {
  final CharacterRemoteSource _remoteSource;

  const CharacterRepository(this._remoteSource);

  @override
  Future<List<Character>> getCharacters({required int page}) async {
    return await _remoteSource.getCharacters(page);
  }
}
