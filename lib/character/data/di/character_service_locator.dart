import '../../../common/graphql_client.dart';
import '../remote/character_remote_source.dart';
import '../repository/character_repository.dart';

final characterRemoteSource = CharacterRemoteSource(graphQLClient);
final characterRepository = CharacterRepository(characterRemoteSource);
