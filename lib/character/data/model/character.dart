import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

@immutable
class Character extends Equatable {
  final String name;
  final String species;
  final String image;
  final String created;

  const Character({
    required this.name,
    required this.species,
    required this.image,
    required this.created,
  });

  factory Character.fromJson(Map<String, dynamic> json) {
    return Character(
      name: json['name'] as String? ?? '',
      species: json['species'] as String? ?? '',
      image: json['image'] as String? ?? '',
      created: json['created'] as String? ?? '',
    );
  }

  @override
  List<Object?> get props => [name, species, created];
}
