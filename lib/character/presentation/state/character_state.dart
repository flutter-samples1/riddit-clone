import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

import '../../data/model/character.dart';

enum CharacterStatus {
  intial,
  loading,
  loaded,
  error;

  bool get isInitial => this == CharacterStatus.intial;
  bool get isLoading => this == CharacterStatus.loading;
  bool get isLoaded => this == CharacterStatus.loaded;
  bool get isError => this == CharacterStatus.error;
}

@immutable
class CharacterState extends Equatable {
  final CharacterStatus status;
  final List<Character> characters;
  final int page;
  final Exception? error;

  const CharacterState({
    this.status = CharacterStatus.intial,
    this.characters = const [],
    this.page = initialPage,
    this.error,
  });

  static const initialPage = 1;

  CharacterState copyWith({
    CharacterStatus? status,
    List<Character>? characters,
    int? page,
    Exception? error,
  }) {
    return CharacterState(
      status: status ?? this.status,
      characters: characters ?? this.characters,
      page: page ?? this.page,
      error: error ?? this.error,
    );
  }

  @override
  List<Object?> get props => [status, characters, error];
}
