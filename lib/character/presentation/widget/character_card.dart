import 'package:flutter/material.dart';

class CharacterCard extends StatelessWidget {
  const CharacterCard({
    Key? key,
    required this.name,
    required this.species,
    required this.image,
    required this.created,
  }) : super(key: key);

  final String name;
  final String species;
  final String image;
  final String created;

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 2,
      child: Row(
        children: [
          Expanded(
            child: Column(
              children: [
                SizedBox(
                  child: Image.network(
                    image,
                    fit: BoxFit.fill,
                    width: double.maxFinite,
                  ),
                ),
                Text(
                  name,
                  style: const TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 30,
                  ),
                ),
                Text(
                  species,
                  style: const TextStyle(
                    fontSize: 14,
                  ),
                  maxLines: 4,
                ),
                const SizedBox(height: 8),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
