// Create an character view for viewing of characters using bloc.
// Every time the user scrolls to the bottom of the list, the next page of characters is fetched.

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_hooks/flutter_hooks.dart';

import '../bloc/character_bloc.dart';
import '../event/character_event.dart';
import '../state/character_state.dart';
import 'character_card.dart';

class CharacterView extends HookWidget {
  const CharacterView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final scrollController = useScrollController();

    return Column(
      children: [
        _ScrollController(scrollController: scrollController),
        BlocBuilder<CharacterBloc, CharacterState>(
          bloc: context.read<CharacterBloc>(),
          builder: (context, state) {
            return Expanded(
              child: ListView.builder(
                controller: scrollController,
                itemCount: state.characters.length + 1,
                itemBuilder: (context, index) {
                  if (index >= state.characters.length) {
                    return const Center(
                      child: CircularProgressIndicator(),
                    );
                  }

                  final character = state.characters[index];
                  return CharacterCard(
                    name: character.name,
                    species: character.species,
                    image: character.image,
                    created: character.created,
                  );
                },
              ),
            );
          },
        ),
      ],
    );
  }
}

class _ScrollController extends HookWidget {
  const _ScrollController({required this.scrollController});

  final ScrollController scrollController;

  @override
  Widget build(BuildContext context) {
    void scrollListener() {
      if (scrollController.offset >=
          scrollController.position.maxScrollExtent) {
        context.read<CharacterBloc>().add(CharactersFetched());
      }
    }

    useEffect(() {
      scrollController.addListener(scrollListener);
      return () => scrollController.removeListener(scrollListener);
    }, [scrollController]);

    return const SizedBox();
  }
}
