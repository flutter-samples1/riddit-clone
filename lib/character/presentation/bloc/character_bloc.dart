import 'package:bloc/bloc.dart';

import '../../data/interface/character_interface.dart';
import '../event/character_event.dart';
import '../state/character_state.dart';

class CharacterBloc extends Bloc<CharacterEvent, CharacterState> {
  final CharacterInterface _characterInterface;

  CharacterBloc({
    required CharacterInterface repository,
  })  : _characterInterface = repository,
        super(const CharacterState()) {
    on<CharactersFetched>(_mapCharactersInitializedToState);
  }

  Future<void> _mapCharactersInitializedToState(
    CharactersFetched event,
    Emitter<CharacterState> emit,
  ) async {
    try {
      emit(state.copyWith(status: CharacterStatus.loading));

      final characters =
          await _characterInterface.getCharacters(page: state.page);

      emit(state.copyWith(
        status: CharacterStatus.loaded,
        characters: [...characters, ...state.characters],
        page: state.page + 1,
      ));
    } on Exception catch (error) {
      emit(state.copyWith(status: CharacterStatus.error, error: error));
    }
  }
}
