import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../blog/presentation/screens/blog_screen.dart';
import '../../../common/presentation/screen/my_drawer.dart';
import '../../../common/presentation/widget/appBar.dart';
import '../../data/di/character_service_locator.dart';
import '../bloc/character_bloc.dart';
import '../event/character_event.dart';
import '../widget/character_view.dart';

class CharacterScreen extends StatelessWidget {
  const CharacterScreen({Key? key}) : super(key: key);
  static const id = 'character_screen';
  static const title = 'RicknMorty';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const appBar(switchTo: BlogScreen.id),
      drawer: const MyDrawer(),
      body: BlocProvider(
        create: (_) => CharacterBloc(
          repository: characterRepository,
        )..add(CharactersFetched()),
        child: const CharacterView(),
      ),
    );
  }
}
