import 'package:graphql_flutter/graphql_flutter.dart';

final _httpLink = HttpLink('https://rickandmortyapi.com/graphql');

final GraphQLClient graphQLClient = GraphQLClient(
  cache: GraphQLCache(),
  link: Link.from([_httpLink]),
  defaultPolicies: DefaultPolicies(
    query: Policies.safe(
      FetchPolicy.networkOnly,
      ErrorPolicy.none,
      CacheRereadPolicy.ignoreAll,
    ),
  ),
);
