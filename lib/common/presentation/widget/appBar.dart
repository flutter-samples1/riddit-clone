import 'package:flutter/material.dart';

class appBar extends StatelessWidget implements PreferredSizeWidget {
  const appBar({Key? key, required this.switchTo}) : super(key: key);

  final String switchTo;

  @override
  Widget build(BuildContext context) {
    return AppBar(
      title: const Text('RIDDIT'),
      actions: [
        IconButton(
          icon: const Icon(Icons.switch_access_shortcut_rounded),
          onPressed: () => Navigator.of(context).pushReplacementNamed(switchTo),
        ),
      ],
    );
  }

  @override
  Size get preferredSize => const Size.fromHeight(kToolbarHeight);
}
