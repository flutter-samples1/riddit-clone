import 'package:flutter/material.dart';
import 'package:riddit/blog/presentation/screens/blog_screen.dart';

import '../../../character/presentation/screen/character_screen.dart';
import '../../bloc_export.dart';

class MyDrawer extends StatelessWidget {
  const MyDrawer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Drawer(
        child: Column(
          children: [
            Container(
              width: double.infinity,
              padding: const EdgeInsets.symmetric(vertical: 14, horizontal: 20),
              child: Text(
                'Switch to',
                style: Theme.of(context).textTheme.headlineMedium,
              ),
            ),
            GestureDetector(
              onTap: () =>
                  Navigator.of(context).pushReplacementNamed(BlogScreen.id),
              child: const ListTile(
                leading: Icon(Icons.feed),
                title: Text('View Feed'),
              ),
            ),
            GestureDetector(
              onTap: () => Navigator.of(context)
                  .pushReplacementNamed(CharacterScreen.id),
              child: const ListTile(
                leading: Icon(Icons.folder_special),
                title: Text('Rick and Morty'),
              ),
            ),
            BlocBuilder<SwitchBloc, SwitchState>(
              builder: (context, state) {
                return Switch(
                  value: state.switchValue,
                  onChanged: (newValue) {
                    newValue
                        ? context.read<SwitchBloc>().add(SwitchOnEvent())
                        : context.read<SwitchBloc>().add(SwitchOffEvent());
                  },
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}
