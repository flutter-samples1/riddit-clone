import 'package:flutter/material.dart';
import 'package:riddit/blog/presentation/screens/blog_screen.dart';

import '../character/presentation/screen/character_screen.dart';

class AppRouter {
  Route? onGenerateRoute(RouteSettings routeSettings) {
    switch (routeSettings.name) {
      case BlogScreen.id:
        return MaterialPageRoute(builder: (_) => BlogScreen());
      case CharacterScreen.id:
        return MaterialPageRoute(builder: (_) => CharacterScreen());
      default:
        return null;
    }
  }
}
