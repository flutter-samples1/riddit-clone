// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

@immutable
class Post extends Equatable {
  final String id;
  final String title;
  final String description;
  final String createdAt;
  bool? isDeleted;

  Post({
    required this.id,
    required this.title,
    required this.description,
    required this.createdAt,
    this.isDeleted,
  }) {
    isDeleted = isDeleted ?? false;
  }

  Post copyWith({
    String? id,
    String? title,
    String? description,
    String? createdAt,
    bool? isDeleted,
  }) {
    return Post(
      id: id ?? this.id,
      title: title ?? this.title,
      description: description ?? this.description,
      createdAt: createdAt ?? this.createdAt,
      isDeleted: isDeleted ?? this.isDeleted,
    );
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'id': id,
      'title': title,
      'description': description,
      'createdAt': createdAt,
      'isDeleted': isDeleted,
    };
  }

  factory Post.fromMap(Map<String, dynamic> map) {
    return Post(
      id: map['id'] as String,
      title: map['title'] as String,
      description: map['description'] as String,
      createdAt: map['createdAt'] as String,
      isDeleted: map['isDeleted'] != null ? map['isDeleted'] as bool : null,
    );
  }

  @override
  // TODO: implement props
  List<Object?> get props => [id, title, description, createdAt, isDeleted];
}
