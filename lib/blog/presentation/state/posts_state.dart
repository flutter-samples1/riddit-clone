// ignore_for_file: public_member_api_docs, sort_constructors_first
part of '../bloc/posts_bloc.dart';

@immutable
class PostsState extends Equatable {
  final List<Post> allPosts;
  const PostsState({this.allPosts = const <Post>[]});

  @override
  List<Object> get props => [allPosts];

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'allPosts': allPosts.map((x) => x.toMap()).toList(),
    };
  }

  factory PostsState.fromMap(Map<String, dynamic> map) {
    // return PostsState(
    //   allPosts: List<Post>.from(
    //     (map['allPosts'] as List<int>).map<Post>(
    //       (x) => Post.fromMap(x as Map<String, dynamic>),
    //     ),
    //   ),
    // );
    return PostsState(
      allPosts: List<Post>.from(
        map['allPosts']?.map((x) => Post.fromMap(x)),
      ),
    );
  }
}
