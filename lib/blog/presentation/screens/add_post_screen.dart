import 'package:flutter/material.dart';
import 'package:moment_dart/moment_dart.dart';
import 'package:riddit/blog/data/model/post.dart';
import '../../../common/bloc_export.dart';
import 'package:uuid/uuid.dart';

final now = DateTime.now();

class AddPostScreen extends StatelessWidget {
  const AddPostScreen({
    super.key,
    required this.titleController,
    required this.descriptionController,
  });

  final TextEditingController titleController;
  final TextEditingController descriptionController;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(20),
      child: Column(
        children: [
          const Text(
            'Create Post',
            style: TextStyle(fontSize: 24),
          ),
          const SizedBox(
            height: 10,
          ),
          TextField(
            controller: titleController,
            decoration: const InputDecoration(
              label: Text('Title'),
              border: OutlineInputBorder(),
            ),
          ),
          const SizedBox(height: 4),
          IntrinsicHeight(
            child: TextField(
              controller: descriptionController,
              maxLines: null,
              decoration: const InputDecoration(
                hintText: "What's on your mind?",
                border: OutlineInputBorder(),
              ),
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              TextButton(
                onPressed: () => Navigator.pop(context),
                child: const Text('Cancel'),
              ),
              ElevatedButton(
                onPressed: () {
                  var uuid = const Uuid();
                  var post = Post(
                    title: titleController.text,
                    description: descriptionController.text,
                    createdAt: DateTime.now().format(),
                    id: uuid.v4(),
                  );
                  context.read<PostsBloc>().add(
                        AddPost(post: post),
                      );
                  Navigator.pop(context);
                },
                child: const Text('Share'),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
