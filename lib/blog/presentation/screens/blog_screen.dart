import 'package:flutter/material.dart';
import 'package:riddit/blog/data/model/post.dart';
import 'package:riddit/common/presentation/screen/my_drawer.dart';

import '../../../character/presentation/screen/character_screen.dart';
import '../../../common/bloc_export.dart';
import '../../../common/presentation/widget/appBar.dart';
import '../widgets/posts_list.dart';
import 'add_post_screen.dart';

class BlogScreen extends StatefulWidget {
  BlogScreen({Key? key}) : super(key: key);
  static const id = 'blog_screen';
  static const title = 'RIDDIT';

  @override
  State<BlogScreen> createState() => _BlogScreenState();
}

class _BlogScreenState extends State<BlogScreen> {
  TextEditingController titleController = TextEditingController();
  TextEditingController descriptionController = TextEditingController();

  void _addPost(BuildContext context) {
    showModalBottomSheet(
      context: context,
      builder: (context) => SingleChildScrollView(
        child: Container(
          padding:
              EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
          child: AddPostScreen(
            titleController: titleController,
            descriptionController: descriptionController,
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<PostsBloc, PostsState>(
      builder: (context, state) {
        List<Post> postsList = state.allPosts;
        return Scaffold(
          appBar: const appBar(switchTo: CharacterScreen.id),
          drawer: const MyDrawer(),
          body: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              PostsList(postsList: postsList),
            ],
          ),
          floatingActionButton: FloatingActionButton(
            onPressed: () => _addPost(context),
            tooltip: 'Add Task',
            child: const Icon(Icons.add),
          ),
        );
      },
    );
  }
}
