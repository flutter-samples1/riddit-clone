part of '../bloc/posts_bloc.dart';

abstract class PostsEvent extends Equatable {
  const PostsEvent();

  @override
  List<Object> get props => [];
}

class AddPost extends PostsEvent {
  final Post post;
  AddPost({
    required this.post,
  });

  @override
  List<Object> get props => [post];
}

class DeletePost extends PostsEvent {
  final Post post;
  DeletePost({
    required this.post,
  });

  @override
  List<Object> get props => [post];
}
