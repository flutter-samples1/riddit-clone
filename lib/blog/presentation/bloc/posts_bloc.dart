import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:riddit/common/bloc_export.dart';
import 'package:riddit/blog/data/model/post.dart';

part '../event/posts_event.dart';
part '../state/posts_state.dart';

class PostsBloc extends HydratedBloc<PostsEvent, PostsState> {
  PostsBloc() : super(const PostsState()) {
    on<AddPost>(_onAddPost);
    on<DeletePost>(_onDeletePost);
  }

  void _onAddPost(AddPost event, Emitter<PostsState> emit) {
    final state = this.state;
    emit(
      PostsState(allPosts: List.from(state.allPosts)..add(event.post)),
    );
  }

  void _onDeletePost(DeletePost event, Emitter<PostsState> emit) {
    final state = this.state;
    emit(
      PostsState(allPosts: List.from(state.allPosts)..remove(event.post)),
    );
  }

  @override
  PostsState? fromJson(Map<String, dynamic> json) {
    return PostsState.fromMap(json);
  }

  @override
  Map<String, dynamic>? toJson(PostsState state) {
    return state.toMap();
  }
}
