import 'package:flutter/material.dart';
import '../../data/model/post.dart';
import 'post_tile.dart';

class PostsList extends StatelessWidget {
  const PostsList({
    super.key,
    required this.postsList,
  });

  final List<Post> postsList;

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: ListView.builder(
          itemCount: postsList.length,
          itemBuilder: (context, index) {
            var post = postsList[index];
            return PostTile(post: post);
          }),
    );
  }
}
